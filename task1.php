    <?php
    #bai1
    #a)tính tổng số chẵn
    $sumeven = 0;
    for ($i = 2; $i <= 100; $i += 2) {
        $sumeven += $i;
    }

    #b) tính tổng số lẻ
    $sumodd = 0;
    for ($i = 1; $i <= 100; $i += 2) {
        $sumodd += $i;
    }

    #c) đếm số chẵn
    $counteven = 0;
    for ($i = 1; $i <= 100; $i++) {
        if ($i % 2) {
            $counteven++;
        }
    }

    #d) đếm số lẻ
    $countodd = 0;
    for ($i = 1; $i <= 100; $i++) {
        if ($i % 2 != 0) {
            $countodd++;
        }
    }
    
    #Bài 2 ($ _GET và $ _POST được sử dụng để thu thập dữ liệu biểu mẫu)
    #a)
    if (isset($_POST["number"])) { //isset kiểm tra xem đã nhập giá trị vào chưa 
        $x = $_POST["number"];
        $sumeven = 0;
        $sumodd = 0;
        for ($i = 0; $i <= $x; $i += 2) {
            $sumeven += $i;
        }

        for ($i = 1; $i <= $x; $i += 2) {
            $sumodd += $i;
        }
    } else {
        echo "chưa nhập đủ giá trị<br>";
    }
   
    #Bài 3 : bài tập đổi vị trí 2 phần tử
    if(isset($_POST["numbera"]) && isset($_POST["numberb"])) {
        $a = $_POST["numbera"];
        $b =  $_POST["numberb"];
        $temp;
    
        $temp = $a;
        $a = $b;
        $b = $temp;
    } else {
        echo "chưa nhập giá trị của a và b <br>";
    }
    
    #Bài 4: bài tập về if-else
    if(isset($_POST["name"])) {
        $name = strtolower($_POST["name"]);
        //strcasecmp so sánh không phân biệt chữ hoa , chữ thường;
        if (strcasecmp($name, "nguyễn trung hiếu") == 0) {
            echo "Người hướng dẫn <br>";
        } else {
            echo "Thực tập sinh <br>";
    }
    }else {
        echo "chưa nhập tên <br>";
    }
  
    if(isset($_POST["month"])) {
        $month = $_POST["month"];
        switch ($month) {
            case "1":
            case "3":
            case "5":
            case "7":
            case "8":
            case "10":
            case "12":
                echo "tháng có 31 ngày <br>";
                break;
            case "2":
                echo "tháng có 28 hoặc 29 ngày <br>";
                break;
            case "4":
            case "6":
            case "9":
            case "11":
                echo "tháng có 30 ngày <br>";
                break;
            default:
                echo "chỉ có 12 tháng <br>";
        }
    }else {
        echo "chưa nhập tháng <br>";
    }
    ?>